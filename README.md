# Essential Classic Games & website

Stats are now displayed on the web! Visit us and try the new messaging app ➤ https://www.stawn.live  
You have the right to claim the deletion of your user account. Contact me here for any question or request, or send a message to the [Admin](https://www.stawn.live/messages/create) through the website.  
Only 3 cookies are used on the site: one which keeps you logged in between pages, the 'Remember me' cookie, and a token cookie to prevent another malicious user from sending illegal data to the server.  

## Requirements
To play games, you need Java SDK 16 minmum ➤ https://www.oracle.com/fr/java/technologies/downloads/  
When you have installed it, download Essential Classic Games and double click on the .exe launcher. ➤ [Direct Download](https://stawn.live/app_dl/Essential_Classic_Games_v2.5.zip)  
If you don't download the full repository from GitLab, **you need to take resources folder too!** This folder must be placed right besides the .jar or the .exe file.  

## Notes
The game launcher uses **NORM** SQL implementation (Copyright 2014, Dieselpoint, Inc. under Apache License v2.0).  
Pong based on Gaspared's and William's codes, and Flappy on Mompi's and Pinkman's games.  
Leaderboard displays **even scores** based on your id or your name, so the sorting a visiter sees may vary **in this case**.

## Change log
09/03/22: Reinforced DB encryption.  
27/02/22: Modified players DB to strict minimum.  
25/08/21: Added Stats button (v2.3).  
09/08/21: Added data encryption, you will need to re-create an account.  
		  It now has Flappy bird game! Thx to Mompi's and Pinkman.  
		  I added an easter egg at 50 points. **Once you finish the game, you can only get more points by finishing it again, or at least half of the run.**